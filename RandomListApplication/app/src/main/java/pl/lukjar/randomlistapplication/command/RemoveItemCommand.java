package pl.lukjar.randomlistapplication.command;

import android.support.annotation.NonNull;
import android.util.Log;

import java.util.List;
import java.util.Random;

import pl.lukjar.randomlistapplication.model.ListItem;

public class RemoveItemCommand extends ItemsCommand {

    private final Random random = new Random();

    @Override
    public int execute(@NonNull List<ListItem> items) {
        int itemIndexToRemove = random.nextInt(items.size());
        items.remove(itemIndexToRemove);
        Log.d(TAG, "Remove item on index " + itemIndexToRemove);
        return itemIndexToRemove;
    }
}
