package pl.lukjar.randomlistapplication.command;

import android.support.annotation.NonNull;
import android.util.Log;

import java.util.List;
import java.util.Random;

import pl.lukjar.randomlistapplication.model.ListItem;

public class IncreaseCounterWithPreviousItemCounterValueCommand extends ItemsCommand {
    private final Random random = new Random();

    @Override
    public int execute(@NonNull List<ListItem> items) {
        int randomIndex = random.nextInt(items.size());
        int previousElementIndex = getPreviousElementIndex(randomIndex);

        if (randomIndex != previousElementIndex) {
            ListItem itemToModify = items.get(randomIndex);
            int counterDelta = items.get(previousElementIndex).getCount();
            itemToModify.setCount(itemToModify.getCount() + counterDelta);
            Log.d(TAG, "Increase counter on index " + randomIndex + " by " + counterDelta);
            return randomIndex;
        }

        return -1;
    }

    private int getPreviousElementIndex(int currentIndex) {
        return currentIndex == 0 ? 0 : --currentIndex;
    }
}
