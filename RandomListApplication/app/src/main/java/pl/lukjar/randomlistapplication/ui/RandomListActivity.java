package pl.lukjar.randomlistapplication.ui;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;

import org.parceler.Parcels;

import nucleus.factory.RequiresPresenter;
import nucleus.view.NucleusAppCompatActivity;
import pl.lukjar.randomlistapplication.R;
import pl.lukjar.randomlistapplication.command.ItemsCommand;
import pl.lukjar.randomlistapplication.databinding.ActivityRandomListBinding;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.Subscriptions;

@RequiresPresenter(RandomListPresenter.class)
public class RandomListActivity extends NucleusAppCompatActivity<RandomListPresenter> {
    private static final String WAS_RUNNING = "was_running";
    private static final String LIST_ITEMS = "list_items";

    private ActivityRandomListBinding binding;
    private Subscription oneSecondEventsSubscription = Subscriptions.empty();
    private boolean isRunning = false;
    private ListAdapter listAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_random_list);
        binding.floatingActionButton.setOnClickListener(this::onFloatingActionButtonClick);
        binding.resetButton.setOnClickListener(this::onResetButtonClick);
        listAdapter = new ListAdapter();
        binding.recyclerView.setAdapter(listAdapter);
        binding.recyclerView.setHasFixedSize(true);

        if (savedInstanceState != null) {
            listAdapter.addAll(Parcels.unwrap(savedInstanceState.getParcelable(LIST_ITEMS)));
        }
    }

    private void onResetButtonClick(View view) {
        if (isRunning) {
            stopObservingEvents();
        }
        listAdapter.clear();
    }

    private void onFloatingActionButtonClick(View view) {
        if (isRunning) {
            stopObservingEvents();
        } else {
            startObservingEvents();
        }
    }

    private void startObservingEvents() {
        startObservingOneSecondEvents();
        binding.floatingActionButton.setImageResource(R.drawable.ic_pause);
        isRunning = true;
    }

    private void stopObservingEvents() {
        oneSecondEventsSubscription.unsubscribe();
        binding.floatingActionButton.setImageResource(R.drawable.ic_play_arrow);
        isRunning = false;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putBoolean(WAS_RUNNING, isRunning);
        outState.putParcelable(LIST_ITEMS, Parcels.wrap(listAdapter.getItems()));
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        if (savedInstanceState.getBoolean(WAS_RUNNING)) {
            startObservingEvents();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        oneSecondEventsSubscription.unsubscribe();
    }

    private void startObservingOneSecondEvents() {
        oneSecondEventsSubscription = getPresenter().getEvents()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::updateListOnOneSecondEvent);
    }

    private void updateListOnOneSecondEvent(@NonNull ItemsCommand itemsCommand) {
        if (listAdapter.size() < 5) {
            listAdapter.addItem(getPresenter().createListItem(this));
        } else {
            listAdapter.modifyWithCommand(itemsCommand);
        }
    }
}
