package pl.lukjar.randomlistapplication.command;

import android.support.annotation.NonNull;
import android.util.Log;

import java.util.List;
import java.util.Random;

import pl.lukjar.randomlistapplication.model.ListItem;

public class IncreaseCounterByOneCommand extends ItemsCommand {
    private final Random random = new Random();

    @Override
    public int execute(@NonNull List<ListItem> items) {
        int itemToModifyIndex = random.nextInt(items.size());
        increaseListItemCount(items.get(itemToModifyIndex));
        Log.d(TAG, "Increase counter on index " + itemToModifyIndex + " by one");
        return itemToModifyIndex;
    }

    private void increaseListItemCount(@NonNull ListItem listItem) {
        listItem.setCount(listItem.getCount() + 1);
    }
}
