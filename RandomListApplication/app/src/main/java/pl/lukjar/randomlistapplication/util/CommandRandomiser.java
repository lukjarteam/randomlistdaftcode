package pl.lukjar.randomlistapplication.util;

import android.support.annotation.NonNull;
import android.util.Range;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import pl.lukjar.randomlistapplication.command.ItemsCommand;

public class CommandRandomiser {
    private int weightSum;
    private Random random = new Random();
    private Map<Range<Integer>, ItemsCommand> commandsProbabilityMap = new HashMap<>();

    public CommandRandomiser addCommand(ItemsCommand itemsCommand, int weight) {
        commandsProbabilityMap.put(Range.create(weightSum, weightSum + weight), itemsCommand);
        weightSum += weight;
        return this;
    }

    @NonNull
    public ItemsCommand randomCommand() {
        int randomNumber = random.nextInt(weightSum);

        for (Map.Entry<Range<Integer>, ItemsCommand> entry : commandsProbabilityMap.entrySet()) {
            if (entry.getKey().contains(randomNumber)) {
                return entry.getValue();
            }
        }

        throw new IllegalStateException("Command not found");
    }
}
