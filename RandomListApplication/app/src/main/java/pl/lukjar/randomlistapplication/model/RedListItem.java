package pl.lukjar.randomlistapplication.model;

import android.content.Context;
import android.support.annotation.ColorInt;
import android.support.v4.content.ContextCompat;

import org.parceler.Parcel;
import org.parceler.ParcelConstructor;

import pl.lukjar.randomlistapplication.R;

import static org.parceler.Parcel.Serialization.BEAN;

@Parcel(BEAN)
public class RedListItem extends ListItem {

    @ParcelConstructor
    public RedListItem(@ColorInt int color, int count) {
        super(color, count);
    }

    public RedListItem(Context context) {
        super(ContextCompat.getColor(context, R.color.red), 0);
    }

    @Override
    public int getDisplayCount() {
        return getCount();
    }
}
