package pl.lukjar.randomlistapplication.command;

import android.support.annotation.NonNull;
import android.util.Log;

import java.util.List;
import java.util.Random;

import pl.lukjar.randomlistapplication.model.ListItem;

public class ResetItemCounterCommand extends ItemsCommand {

    private final Random random = new Random();

    @Override
    public int execute(@NonNull List<ListItem> items) {
        int itemToModifyIndex = random.nextInt(items.size());
        resetCounter(items.get(itemToModifyIndex));
        Log.d(TAG, "Reset counter on index " + itemToModifyIndex);
        return itemToModifyIndex;
    }

    private void resetCounter(@NonNull ListItem listItem) {
        listItem.setCount(0);
    }
}
