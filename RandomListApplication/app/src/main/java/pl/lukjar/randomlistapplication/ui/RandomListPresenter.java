package pl.lukjar.randomlistapplication.ui;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.CheckResult;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.Random;

import nucleus.presenter.Presenter;
import pl.lukjar.randomlistapplication.model.BlueListItem;
import pl.lukjar.randomlistapplication.model.ListItem;
import pl.lukjar.randomlistapplication.model.RedListItem;
import pl.lukjar.randomlistapplication.util.CommandRandomiser;
import pl.lukjar.randomlistapplication.command.IncreaseCounterByOneCommand;
import pl.lukjar.randomlistapplication.command.IncreaseCounterWithPreviousItemCounterValueCommand;
import pl.lukjar.randomlistapplication.command.ItemsCommand;
import pl.lukjar.randomlistapplication.command.RemoveItemCommand;
import pl.lukjar.randomlistapplication.command.ResetItemCounterCommand;
import rx.Observable;

import static java.util.concurrent.TimeUnit.SECONDS;

public class RandomListPresenter extends Presenter<RandomListActivity> {
    private CommandRandomiser commandRandomiser = new CommandRandomiser();

    @Override
    protected void onCreate(@Nullable Bundle savedState) {
        super.onCreate(savedState);
        commandRandomiser
                .addCommand(new IncreaseCounterByOneCommand(), 50)
                .addCommand(new ResetItemCounterCommand(), 35)
                .addCommand(new RemoveItemCommand(), 10)
                .addCommand(new IncreaseCounterWithPreviousItemCounterValueCommand(), 5);
    }

    @CheckResult
    @NonNull
    public Observable<ItemsCommand> getEvents() {
        return Observable.interval(1, SECONDS)
                .map(interval -> randomCommand());
    }

    @NonNull
    public ListItem createListItem(Context context) {
        return new Random().nextBoolean() ? new BlueListItem(context) : new RedListItem(context);
    }

    @NonNull
    private ItemsCommand randomCommand() {
        return commandRandomiser.randomCommand();
    }
}
