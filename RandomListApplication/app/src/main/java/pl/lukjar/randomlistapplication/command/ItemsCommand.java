package pl.lukjar.randomlistapplication.command;

import android.support.annotation.NonNull;

import java.util.List;

import pl.lukjar.randomlistapplication.model.ListItem;

public abstract class ItemsCommand {
    protected static final String TAG = "COMMAND";

    public abstract int execute(@NonNull List<ListItem> items);
}
