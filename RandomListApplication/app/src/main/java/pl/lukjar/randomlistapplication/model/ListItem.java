package pl.lukjar.randomlistapplication.model;

import android.support.annotation.ColorInt;

import org.parceler.Parcel;
import org.parceler.ParcelConstructor;

import static org.parceler.Parcel.Serialization.BEAN;

@Parcel(BEAN)
public class ListItem {
    private int color;
    private int count;

    @ParcelConstructor
    public ListItem(@ColorInt int color, int count) {
        this.color = color;
        this.count = count;
    }

    @ColorInt
    public int getColor() {
        return color;
    }

    public int getCount() {
        return count;
    }

    public int getDisplayCount() {
        return getCount();
    }

    public void setCount(int count) {
        this.count = count;
    }
}
