package pl.lukjar.randomlistapplication.ui;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import pl.lukjar.randomlistapplication.command.ItemsCommand;
import pl.lukjar.randomlistapplication.databinding.ListItemBinding;
import pl.lukjar.randomlistapplication.model.ListItem;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder> {
    private List<ListItem> items = new ArrayList<>();

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(ListItemBinding.inflate(LayoutInflater.from(parent.getContext()),
                parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.binding.setItem(items.get(position));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void modifyWithCommand(@NonNull ItemsCommand itemsCommand) {
        int itemsSize = items.size();
        int modifiedItemIndex = itemsCommand.execute(items);

        if (itemsSize > items.size() && modifiedItemIndex != -1) {
            notifyItemRemoved(modifiedItemIndex);
        } else {
            notifyDataSetChanged();
        }
    }

    @NonNull
    public List<ListItem> getItems() {
        return items;
    }

    public void addAll(@NonNull List<ListItem> items) {
        this.items.addAll(items);
        notifyDataSetChanged();
    }

    public void clear() {
        items.clear();
        notifyDataSetChanged();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        ListItemBinding binding;

        public ViewHolder(ListItemBinding listItemBinding) {
            super(listItemBinding.getRoot());
            binding = listItemBinding;
        }
    }

    public void addItem(@NonNull ListItem listItem) {
        items.add(listItem);
        notifyItemInserted(items.size());
    }

    public int size() {
        return items.size();
    }
}
